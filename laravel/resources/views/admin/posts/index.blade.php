@extends('layouts.admin')


@section('content')

  @if(Session::has('deleted_post'))

    <p class="bg-danger">{{ session('deleted_post') }}</p>

  @endif

  <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>Photo</th>
        <th>User</th>
        <th>Category</th>
        <th>Title</th>
        <th>Body</th>
        <th>Created</th>
      </tr>
    </thead>
    <tbody>
    @if($posts)
      @foreach($posts as $post)
      <tr>
        <td>{{ $post->id }}</td>
        <td><img height="50" src="{{ $post->photo ? $post->photo->file : 'http://placehold.it/400x400' }}" alt=""></td>
        <td>{{ $post->user->name }}</td>
        <td>{{ $post->category ? $post->category->name : 'Uncategorized' }}</td>
        <td><a href="/admin/posts/{{ $post->id }}/edit">{{ $post->title }}</a></td>
        <td>{{ str_limit($post->body, 10) }}</td>
        <td>{{ $post->created_at->diffForhumans() }}</td>
      </tr>
      @endforeach
    @endif
  </tbody>
</table>

@stop
